<!DOCTYPE html>
<html>
	<head>
		<html lang="pl-PL">
		<meta charset="utf-8">
		<title> TEST </title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div class = "baner">
			<h1><i> Baza danych DDK - kopia </i></h1>
		</div>
		<div class = "lewy">
			<p class="main">Wybierz tryb użytkownika:</p>
			<p class = "login">
				<img src="oneko_stop.png" alt="Zwykły użytkownik" usemap="#1">
				<map id="1">
					<area shape="rect" coords="0, 0, 32, 32" href="http://localhost/DDK%20-%20strona%20internetowa/user.php" alt="user">
				</map>
				<br>Zwykły użytkownik
			</p>
			<p class = "login">
				<img src="oneko_cat.png" alt="Administrator" usemap="#2" onclick="logon()">
				<br>Administrator (wymagane logowanie)
				<script type="text/javascript">
					var login = "admin";
					var password = "admin";
				function logon()
				{
					var l = prompt("Login: ");
					var p = prompt("Hasło: ");
					if(l == login && p == password)
					{
						alert("Logowanie zakończone sukcesem.");
						document.location.href="http://localhost/DDK%20-%20strona%20internetowa/privileged_user.php";
					}
					else
						alert("Wprowadzono niepoprawne dane. Spróbuj ponownie.");
					
				}
				
				</script>
			</p>
		</div>
		<div class = "prawy">
			<h3> Statystyka bazy danych DDK (Dane Do Kont) </h3>
			Ilość tabel w bazie danych: <b> 3. </b><br>
			<?php
				$connect = mysqli_connect('localhost', 'root', '', 'kopia_ddk');
			
				if($connect)
				{
					$query = "SELECT count(id) FROM dane";
					$result = mysqli_query($connect, $query);
					
					while($row = mysqli_fetch_array($result))
						echo "Aktualnie baza danych zawiera: <b>".$row[0]."</b> rekordów.";
				}
				mysqli_close($connect);
			?>
			<br><br><h3> Struktura DDK </h3>
			<list>
				<ul type = "none">
					<li><b> Właściciel danych (wlasciciel_danych) <br><br></b>
						<table>
							<tr id = "naglowek">
								<td class = "td_naglowek"><b> Nazwa </b></td>
								<td class = "td_naglowek"><b> Typ </b></td>
								<td class = "td_naglowek"><b> Specjalne </b></td>
							</tr>
							<tr>
								<td> id </td>
								<td> int(11) </td>
								<td> Klucz główny (Auto_Increment, not NULL)</td>
							</tr>
							<tr>
								<td> wlasciciel </td>
								<td> varchar(40) </td>
								<td> Wartość domyślna: NULL</td>
							</tr>
						</table>
					</li>
					<li><b><br> Rodzaj danych (rodzaj_danych) <br><br></b>
						<table>
							<tr id = "naglowek">
								<td class = "td_naglowek"><b> Nazwa </b></td>
								<td class = "td_naglowek"><b> Typ </b></td>
								<td class = "td_naglowek"><b> Specjalne </b></td>
							</tr>
							<tr>
								<td> id </td>
								<td> int(11) </td>
								<td> Klucz główny (Auto_Increment, not NULL)</td>
							</tr>
							<tr>
								<td> rodzaj </td>
								<td> varchar(40) </td>
								<td> Wartość domyślna: NULL</td>
							</tr>
						</table>
					</li>
					<li><b><br> Dane (dane) <br><br></b>
					<table>
							<tr id = "naglowek">
								<td class = "td_naglowek"><b> Nazwa </b></td>
								<td class = "td_naglowek"><b> Typ </b></td>
								<td class = "td_naglowek"><b> Specjalne </b></td>
							</tr>
							<tr>
								<td> id </td>
								<td> int(11) </td>
								<td> Klucz główny (Auto_Increment, not NULL)</td>
							</tr>
							<tr>
								<td> id_wlasciciela </td>
								<td> int(11) </td>
								<td> Klucz obcy (w relacji z polem <b>id</b> z tabeli <b>wlasciciel_danych</b>)</td>
							</tr>
							<tr>
								<td> id_rodzaju </td>
								<td> int(11) </td>
								<td> Klucz obcy (w relacji z polem <b>id</b> z tabeli <b>rodzaj_danych</b>)</td>
							</tr>
							<tr>
								<td> strona_internetowa </td>
								<td> varchar(50) </td>
								<td> Wartość domyślna: NULL</td>
							</tr>
							<tr>
								<td> login </td>
								<td> haslo(50) </td>
								<td> Wartość domyślna: NULL</td>
							</tr>
							<tr>
								<td> haslo </td>
								<td> varchar(50) </td>
								<td> Wartość domyślna: NULL</td>
							</tr>
							<tr>
								<td> opis </td>
								<td> varchar(400) </td>
								<td> Wartość domyślna: "Brak opisu"</td>
							</tr>
						</table>
					</li>
				</ul>
			</list>
		</div>
		<div class = "stopka">
			<i><b> Strona internetowa autorstwa:
			<a href="mailto:suzume4yuu@gmail.com"> SuzumeYuu :3 </a></b></i>
		</div>
	</body>
</html>
