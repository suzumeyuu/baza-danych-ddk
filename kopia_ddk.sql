-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 08 Sie 2022, 13:04
-- Wersja serwera: 10.4.24-MariaDB
-- Wersja PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `kopia_ddk`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dane`
--

CREATE TABLE `dane` (
  `id` int(11) NOT NULL,
  `id_wlasciciela` int(11) DEFAULT NULL,
  `id_rodzaju` int(11) DEFAULT NULL,
  `strona_internetowa` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `haslo` varchar(50) DEFAULT NULL,
  `opis` varchar(400) DEFAULT 'Brak opisu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `dane`
--

INSERT INTO `dane` (`id`, `id_wlasciciela`, `id_rodzaju`, `strona_internetowa`, `login`, `haslo`, `opis`) VALUES
(156, 1, 1, 'poczta.wp.pl', 'example@wp.pl', '12345678', 'Konto e-mail do wp.pl'),
(157, 2, 2, 'eldarya.pl', 'login', 'haslo', 'Brak opisu'),
(158, 3, 3, 'strona.internetowa.com', 'login123', 'haslo123', 'Brak opisu'),
(159, 1, 4, 'facebook.com', 'email@email.com', 'haslo456', 'Dane do konta fb'),
(160, 2, 5, 'cufs.vulcan.net.pl', 'uczen1', 'haslo789', 'Dane do logowania do e-dziennika'),
(161, 3, 6, 'allegro.pl', 'sprzedawca123', 'a1amak0ta', 'Konto do zakupow na Allegro');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rodzaj_danych`
--

CREATE TABLE `rodzaj_danych` (
  `id` int(11) NOT NULL,
  `rodzaj` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `rodzaj_danych`
--

INSERT INTO `rodzaj_danych` (`id`, `rodzaj`) VALUES
(1, 'email'),
(2, 'gra'),
(3, 'pozostale'),
(4, 'spolecznosc'),
(5, 'szkola / nauka'),
(6, 'zakupy'),
(7, 'studia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wlasciciel_danych`
--

CREATE TABLE `wlasciciel_danych` (
  `id` int(11) NOT NULL,
  `wlasciciel` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `wlasciciel_danych`
--

INSERT INTO `wlasciciel_danych` (`id`, `wlasciciel`) VALUES
(1, 'wlasciciel1'),
(2, 'wlasciciel2'),
(3, 'wlasciciel3');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dane`
--
ALTER TABLE `dane`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_wlasciciela` (`id_wlasciciela`),
  ADD KEY `id_rodzaju` (`id_rodzaju`);

--
-- Indeksy dla tabeli `rodzaj_danych`
--
ALTER TABLE `rodzaj_danych`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `wlasciciel_danych`
--
ALTER TABLE `wlasciciel_danych`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `dane`
--
ALTER TABLE `dane`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT dla tabeli `rodzaj_danych`
--
ALTER TABLE `rodzaj_danych`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `wlasciciel_danych`
--
ALTER TABLE `wlasciciel_danych`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `dane`
--
ALTER TABLE `dane`
  ADD CONSTRAINT `dane_ibfk_1` FOREIGN KEY (`id_wlasciciela`) REFERENCES `wlasciciel_danych` (`id`),
  ADD CONSTRAINT `dane_ibfk_2` FOREIGN KEY (`id_rodzaju`) REFERENCES `rodzaj_danych` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
