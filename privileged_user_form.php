<!DOCTYPE html>
<html>
	<head>
		<html lang="pl-PL">
		<meta charset="utf-8">
		<title> TEST </title>
		<link rel="stylesheet" type="text/css" href="style_privileged_user.css">
	</head>
	<body>
		<div class = "baner">
			<h1><i> Baza danych DDK - kopia </i></h1>
		</div>
		<div class = "lewy">
			<p><i> Jesteś zalogowany jako: <b> Administrator </b></i></p>
			<p>
				<a href="http://localhost/DDK%20-%20strona%20internetowa/index.php">
				<button>Wyloguj</button> 
				</a>
				<a href="http://localhost/DDK%20-%20strona%20internetowa/privileged_user.php"> 
				<button>Powrót do poprzedniej strony</button> 
				</a>
			</p>
		</div>
		<div class = "prawy">
			<?php
				$query = "SELECT dane.id, wlasciciel_danych.wlasciciel, rodzaj_danych.rodzaj, strona_internetowa, login,";
				
				$warunek = 0;
				
				$sortuj = $_POST['sortuj'];
				$czyZaszyfrowane = $_POST['czyZaszyfrowane'];
				
				if($czyZaszyfrowane)
					$query = $query . $czyZaszyfrowane;
				else
					$query = $query . " haslo,";
				
				$query = $query . " opis FROM dane INNER JOIN wlasciciel_danych ON wlasciciel_danych.id = dane.id_wlasciciela INNER JOIN rodzaj_danych ON rodzaj_danych.id = dane.id_rodzaju";
				
				if(isset($_POST['poszukiwana_wartosc']))
					$warunek = $_POST['poszukiwana_wartosc'];
					
				if($warunek)
					$query = $query . " AND " . $warunek;
					
				if($sortuj)
					$query  = $query . " ORDER BY dane.id " . $sortuj;
				
				$query = $query . ";";

				$connect = mysqli_connect('localhost', 'root', '', 'kopia_ddk');
			
				if($connect)
				{
					$result = mysqli_query($connect, $query);
					
					echo '<table class = "center">';
					
					echo '<tr id = "naglowek">';
					
					echo '<td class = "td_naglowek"><h4> ID </h4></td>';
					echo '<td class = "td_naglowek"><h4> Właściciel danych </h4></td>';
					echo '<td class = "td_naglowek"><h4> Rodzaj danych </h4></td>';
					echo '<td class = "td_naglowek"><h4> Strona internetowa / Aplikacja </h4></td>';
					echo '<td class = "td_naglowek"><h4> Login </h4></td>';
					echo '<td class = "td_naglowek"><h4> Hasło </h4></td>';
					echo '<td class = "td_naglowek"><h4> Opis </h4></td>';
				   
				    echo '</tr>';
					
					while($row = mysqli_fetch_array($result))
					{
						echo '<tr>';
						
						echo '<td>'.$row[0].'</td>';
						echo '<td>'.$row[1].'</td>';
						echo '<td>'.$row[2].'</td>';
						echo '<td>'.$row[3].'</td>';
						echo '<td>'.$row[4].'</td>';
						echo '<td>'.$row[5].'</td>';
						echo '<td>'.$row[6].'</td>';
						
						echo '</tr>';
					}
					
					echo '</table>';
					echo '<br><br>';
				}
				mysqli_close($connect);
			?>
		</div>
		<div class = "stopka">
			<i><b> Strona internetowa autorstwa:
			<a href="mailto:suzume4yuu@gmail.com"> SuzumeYuu :3 </a></b></i>
		</div>
	</body>
</html>
